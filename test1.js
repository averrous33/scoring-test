// -   `({[]})` => true
//     -   `([][]{})`=> true
//     -   `({)(]){[}]` => false
//     -   `[)()]` => false
// ((([]}}} =

function validasi(data) {
  let separator = data.split("");
  // console.log(seperator);
  for (let i = 0; i < separator.length; i++) {
    if (separator[i] == "(") {
      if (separator[i + 1] == ")") {
        return true;
      }
    } else if (separator[i] == "{") {
      if (separator[i + 1] == "}") {
        return true;
      }
    } else if (separator[i] == "[") {
      if (separator[i + 1] == "]") {
        return true;
      }
    } else {
      return false;
    }
  }
}
console.log(validasi("({[]})"));
console.log(validasi("([][]{})"));
console.log(validasi("({)(]){[}]"));
console.log(validasi("[)()]"));
console.log(validasi("((([]}}}"));

/**Jawaban benarnya */

